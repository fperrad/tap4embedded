#
#   tap4embedded : http://github.com/fperrad/tap4embedded
#
#   Copyright (C) 2016-2017 Francois Perrad.
#
#   tap4embedded is free software; you can redistribute it and/or modify it
#   under the terms of the Artistic License 2.0
#

CWARNS  :=

TARGET_CC       := "C:/Program Files (x86)/Arduino/hardware/tools/avr/bin/avr-gcc.exe"
TARGET_OBJCOPY  := "C:/Program Files (x86)/Arduino/hardware/tools/avr/bin/avr-objcopy.exe"
TARGET_CFLAGS   := -mmcu=atmega328p -std=c99 -Os $(CWARNS) $(INCS)

HOST_CC         := gcc
HOST_CFLAGS     := $(CWARNS) $(INCS)

ifeq ($(VERBOSE),1)
QUIET   :=
ECHO    :=@:
else
QUIET   :=@
ECHO    :=@echo
endif

.DEFAULT_GOAL   := build
TESTS   := $(wildcard *.c)

.PHONY: xbuild
xbuild: $(TESTS:.c=.hex)

.PHONY: build
build: $(TESTS:.c=.exe)

.PHONY: clean
clean:
	-del *.exe *.elf *.hex

.PHONY: realclean
realclean: clean
	-del *.dep

%.elf: %.c
	$(ECHO) CCLD    $@
	$(QUIET)$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) -o $@ $<

%.hex: %.elf
	$(ECHO) OBJCOPY $@
	$(QUIET)$(TARGET_OBJCOPY) -Oihex -R.eeprom $< $@

%.exe: %.c
	$(ECHO) CCLD    $@
	$(QUIET)$(HOST_CC) $(HOST_CFLAGS) $(HOST_LDFLAGS) -o $@ $<

%.dep: %.c
	$(ECHO) DEPEND  $@
	$(QUIET)$(HOST_CC) $(INCS) -MM $< -MP -MT $(basename $<).exe -MT $(basename $<).elf -MT $@ > $@

ifneq ($(MAKECMDGOALS),realclean)
include $(TESTS:.c=.dep)
endif

