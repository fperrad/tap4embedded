/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Francois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#include <assert.h>

#include "_tap.h"

int main(void) {
    TAP_STDOUT_INIT();
    plan(2u);
    PASS("before");
    assert(1);
#if 1
    assert(0 != 0);
    PASS("after, unreacheable");
#else
    PASS("after");
#endif
    return exit_status();
}

