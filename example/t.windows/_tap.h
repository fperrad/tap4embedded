/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Fran�ois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#if defined(__unix__) || defined(_WIN32) || defined(_WIN64)
#define TAP_STDOUT_INIT()
#else
#include "uart.c"
#define TAP_STDOUT_INIT()       stdout_init()
#endif
#include "tap.h"

