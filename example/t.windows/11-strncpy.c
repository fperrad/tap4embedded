/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Francois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#include <string.h>     /* black box */

#include "_tap.h"
#include "tap_helpers.h"


void test_ptr() {
    char buffer[16];
    char *res;

setup:

exercise:
    res = strncpy(&buffer[0], "foo", sizeof buffer);

verify:
    PTR_IS(res, &buffer[0], "returned pointer");

teardown:

    return;
}


void test_str(const char *file, unsigned int line, const char *s, size_t n, const char *expected) {
    char buffer[16];

setup:
    memset(&buffer[0], 0x55, sizeof buffer);

exercise:
    (void)strncpy(&buffer[0], s, n);
#if 0
    buffer[n] = '\0';   /* companion assignment needed by the weakless of strncpy */
#endif

verify:
    string_is(file, line, &buffer[0], expected, expected);

teardown:

    return;
}
#define TEST_STR(s, n, e)       test_str(__FILE__, __LINE__, s, n, e)


int main(void) {
    TAP_STDOUT_INIT();
    plan(7u);

    test_ptr();

    TEST_STR("", 2, "");
    TEST_STR("bar", 4, "bar");
    todo("strncpy does not add a final \\0.", 4u);
    TEST_STR("bar", 3, "bar");
    TEST_STR("bar", 2, "ba");
    TEST_STR("bar", 1, "b");
    TEST_STR("bar", 0, "");

    return exit_status();
}

