/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Francois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#include "strlcpy.c"    /* white box */

#include "_tap.h"
#include "tap_helpers.h"


void test_ret() {
    char buffer[16];
    size_t ret;

setup:

exercise:
    ret = strlcpy(&buffer[0], "foo", sizeof buffer);

verify:
    SIZE_IS(ret, 3, "returned size");

teardown:

    return;
}


void test_str(const char *file, unsigned int line, const char *s, size_t n, const char *expected) {
    char buffer[16];

setup:
    memset(&buffer[0], 0x55, sizeof buffer);

exercise:
    (void)strlcpy(&buffer[0], s, n);

verify:
    string_is(file, line, &buffer[0], expected, expected);

teardown:

    return;
}
#define TEST_STR(s, n, e)       test_str(__FILE__, __LINE__, s, n, e)


int main(void) {
    TAP_STDOUT_INIT();
    plan(6u);

    test_ret();

    TEST_STR("", 2, "");
    TEST_STR("bar", 4, "bar");
    TEST_STR("bar", 3, "ba");
    TEST_STR("bar", 2, "b");
    TEST_STR("bar", 1, "");

    return exit_status();
}

