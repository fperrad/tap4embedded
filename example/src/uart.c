/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Francois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#include "uart.h"
#include "mcu.h"

#define BAUD 38400
#include <util/setbaud.h>

static FILE uart_stdout = FDEV_SETUP_STREAM(uart_putc, NULL, _FDEV_SETUP_WRITE);

void uart_init(void) {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

    UCSR0A &= ~(_BV(U2X0));

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(TXEN0);   /* Enable TX */
}

int uart_putc(char ch, FILE* f) {
    f = f;
    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = ch;
    return 0;
}

void stdout_init(void) {
    uart_init();
    stdout = &uart_stdout;
}
