/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Francois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#ifndef UART_H
#define UART_H

#include "stdc99.h"

extern void uart_init(void);
extern int uart_putc(char ch, FILE* f);
extern void stdout_init(void);

#endif

