/*
**  tap4embedded : http://github.com/fperrad/tap4embedded
**
**  Copyright (C) 2016-2017 Francois Perrad.
**
**  tap4embedded is free software; you can redistribute it and/or modify it
**  under the terms of the Artistic License 2.0
*/

#ifndef MCU_H
#define MCU_H

#include <avr/io.h>

#define F_CPU 16000000UL

#endif

