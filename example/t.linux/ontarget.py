#!/usr/bin/env python3

#
#   tap4embedded : http://github.com/fperrad/tap4embedded
#
#   Copyright (C) 2016-2017 Francois Perrad.
#
#   tap4embedded is free software; you can redistribute it and/or modify it
#   under the terms of the Artistic License 2.0
#

import argparse
import os
import subprocess
import sys
import serial

ARDUINO_DIR = os.getenv('ARDUINO_DIR', '/usr/share/arduino')
PORT = os.getenv('PORT_COM', '/dev/ttyACM0')
BAUDRATE = os.getenv('BAUDRATE', '38400')

AVRDUDE = '{}/hardware/tools/avrdude'.format(ARDUINO_DIR)
AVRDUDE_CONF = '{}/hardware/tools/avrdude.conf'.format(ARDUINO_DIR)

# see section 'uno' in /usr/share/arduino/hardware/arduino/boards.txt
MCU = 'atmega328p'
SPEED = '115200'
PROTOCOL = 'arduino'

def upload(hex):
    print('# Uploading to target ...');
    command = ' '.join([
        AVRDUDE,
        '-C' + AVRDUDE_CONF,
        '-q', '-q',
        '-p' + MCU,
        '-c' + PROTOCOL,
        '-P' + PORT,
        '-b' + SPEED,
        '-D', '-Uflash:w:' + hex + ':i'
    ])
    print('# ' + command)
    subprocess.call(command, shell=True)

def run():
    print('# Running on target ...');
    port = serial.Serial(PORT, baudrate=BAUDRATE, timeout=1.0)
    while True:
        line = port.readline()
        if sys.version_info[0] >= 3:
            line = line.decode('latin')
        if len(line) > 0:
            print(line.rstrip())
        if '# Done with tap4embedded.' in line:
            break

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='upload & run an executable on Arduino board.')
    parser.add_argument('hexfile')
    args = parser.parse_args()
    upload(args.hexfile)
    run()
