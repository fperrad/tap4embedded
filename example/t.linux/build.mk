#
#   tap4embedded : http://github.com/fperrad/tap4embedded
#
#   Copyright (C) 2016-2017 Francois Perrad.
#
#   tap4embedded is free software; you can redistribute it and/or modify it
#   under the terms of the Artistic License 2.0
#

CWARNS  := -Wall -Wextra -Wno-unused-label

TARGET_CC       := avr-gcc
TARGET_OBJCOPY  := avr-objcopy
TARGET_CFLAGS   := -mmcu=atmega328p -std=c99 -Os $(CWARNS) $(INCS)

HOST_CC         := gcc
HOST_CFLAGS     := $(CWARNS) $(INCS)

ifeq ($(VERBOSE),1)
QUIET   :=
ECHO    :=@:
else
QUIET   :=@
ECHO    :=@echo
endif

.DEFAULT_GOAL   := build
TESTS   := $(wildcard *.c)

.PHONY: xbuild
xbuild: $(TESTS:.c=.hex)

.PHONY: build
build: $(TESTS:.c=.out)

.PHONY: clean
clean:
	rm -f *.out *.elf *.hex

.PHONY: realclean
realclean: clean
	rm -f *.dep

%.elf: %.c
	$(ECHO) "CCLD    $@"
	$(QUIET)$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) -o $@ $<

%.hex: %.elf
	$(ECHO) "OBJCOPY $@"
	$(QUIET)$(TARGET_OBJCOPY) -Oihex -R.eeprom $< $@

%.out: %.c
	$(ECHO) "CCLD    $@"
	$(QUIET)$(HOST_CC) $(HOST_CFLAGS) $(HOST_LDFLAGS) -o $@ $<

%.dep: %.c
	$(ECHO) "DEPEND  $@"
	$(QUIET)$(HOST_CC) $(INCS) -MM $< -MP -MT $(basename $<).out -MT $(basename $<).elf -MT $@ > $@

ifneq ($(MAKECMDGOALS),realclean)
include $(TESTS:.c=.dep)
endif

