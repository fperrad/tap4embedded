#!/usr/bin/env perl

use 5.010;
use strict;
use warnings;

use Config::Properties;
use Device::SerialPort;
use Pod::Usage;
use Test::More;

pod2usage( {
    -exitval    => 1,
    -verbose    => 99,
    -sections   => 'SYNOPSIS|DESCRIPTION|ENVIRONMENT VARIABLES',
} ) unless @ARGV == 1;

my $hex = $ARGV[0];

my $ARDUINO_DIR = $ENV{ARDUINO_DIR} || '/usr/share/arduino';
my $BOARD = $ENV{BOARD} || 'uno';
my $PORT = $ENV{PORT_COM} || '/dev/ttyACM0';
my $BAUDRATE = $ENV{BAUDRATE} || 38400;

my $AVRDUDE = "${ARDUINO_DIR}/hardware/tools/avrdude";
my $AVRDUDE_CONF = "${ARDUINO_DIR}/hardware/tools/avrdude.conf";

my $cfgfile = "${ARDUINO_DIR}/hardware/arduino/boards.txt";
open my $fh, '<', $cfgfile
    or BAIL_OUT("Cannot open ${cfgfile}: $!");

my $properties = Config::Properties->new();
$properties->load($fh);
close $fh;

my $name = $properties->getProperty("${BOARD}.name");
BAIL_OUT("Unknown board ${BOARD}") unless $name;
note("target ${name}");

my $MCU = $properties->getProperty("${BOARD}.build.mcu");
my $SPEED = $properties->getProperty("${BOARD}.upload.speed");
my $PROTOCOL = $properties->getProperty("${BOARD}.upload.protocol");
$PROTOCOL = 'stk500v1' if $PROTOCOL eq 'stk500';

sub upload {
    my @cmd = (
        $AVRDUDE,
        "-C${AVRDUDE_CONF}",
        '-q',
        '-q',
        "-p${MCU}",
        "-c${PROTOCOL}",
        "-P${PORT}",
        "-b${SPEED}",
        '-D',
        "-Uflash:w:${hex}:i",
    );
    note('Uploading to target ...');
    note(join ' ', @cmd);
    system(@cmd) == 0
        or BAIL_OUT("command failed ($?): @cmd");
    return;
}

sub run {
    note('Running on target ...');
    my $port = Device::SerialPort->new($PORT);
    BAIL_OUT("Cannot open ${PORT}") unless $port;

    $port->baudrate($BAUDRATE);
    $port->read_const_time(5000); # ms

    my $buf = q{};
    while (1) {
        my ($cnt, $str) = $port->read(1);
        last unless $cnt;
        print $str;
        $buf .= $str;
        last if $buf =~ /# Done with tap4embedded\./ms;
    }
    print "\n";

    $port->close();
    return;
}

upload();
run();

__END__

=head1 NAME

ontarget - upload & run an executable on Arduino board

=head1 SYNOPSIS

    ontarget file.hex

or with C<prove>

    prove --exec=./ontarget *.hex

=head1 DESCRIPTION

B<ontarget> allows to upload an executable on an Arduino board,
and to capture its output.

=head1 ENVIRONMENT VARIABLES

=over

=item ARDUINO_DIR

The default value is C</usr/share/arduino>.

=item BOARD

This value is used as entry in configuration file C<hardware/arduino/boards.txt>.
The default value is C<uno>.

=item PORT_COM

The default value is C</dev/ttyACM0>.

=item BAUDRATE

Specify the baudrate used for sending the TAP output.
The default value is C<38400>.

=back

=head1 DEPENDENCIES

This script requires some modules from CPAN :

=over

=item Config::Properties

=item Device::SerialPort

=back

=head1 BUGS & LIMITATIONS

Tested on Ubuntu with an Uno board.

=head1 LICENSE

Copyright (C) 2016-2017 Francois Perrad.

tap4embedded is free software; you can redistribute it and/or modify it
under the terms of the Artistic License 2.0

=cut
