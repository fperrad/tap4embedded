
tap4embedded
============

*He who would travel happily must travel light &mdash; Antoine de Saint-Exup&eacute;ry*

[![Build Status](https://travis-ci.org/fperrad/tap4embedded.png)](https://travis-ci.org/fperrad/tap4embedded)
[![License](http://img.shields.io/badge/Licence-Artistic2.0-brightgreen.svg)](LICENSE)

Introduction
------------

*tap4embedded* is a TAP producer library designed for C embedded.

It allows a simple and efficient way to write *unit tests* in C.

The output uses the [Test Anything Protocol](http://en.wikipedia.org/wiki/Test_Anything_Protocol),
that allows a compatibility with the Perl QA ecosystem.
For example, [prove](http://search.cpan.org/~andya/Test-Harness/bin/prove) a basic CLI,
or [Smolder](http://search.cpan.org/~wonko/Smolder/) a web-based smoke test aggregator server.

### Design Goals

1. *simple* and *transparent* (ie. no automagic)
2. works with *deeply embedded system* toolchains (ie. *libc* minimalist prerequisites)
3. allows dual targeting (x100 faster on host than on target)
4. reuse a well-known output format which is already handled by most of the *continuous integration* tools

Bibliography
------------

* Ian Langworth, chromatic, [Perl Testing](http://oreilly.com/catalog/9780596100926) O'Reilly, 2005
* James W. Grenning, [Test Driven Development for Embedded C](http://pragprog.com/book/jgade/test-driven-development-for-embedded-c) The Pragmatic Bookshelf, 2009

Install & Example
-----------------

See [INSTALL.md](INSTALL.md).

Copyright and License
---------------------

Copyright (C) 2016-2017 Francois Perrad.

_tap4embedded_ is free software; you can redistribute it and/or modify it
under the terms of the Artistic License 2.0
