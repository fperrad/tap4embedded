#!/usr/bin/env perl

use strict;
use warnings;
use autodie qw(open close);
use Config;
use File::Temp qw(tmpnam);

use Test::More tests => 33 * 2;

sub tap_output_is {
    my ( $code, $expected, $success, $name ) = @_;

  SETUP:
    my $filename     = tmpnam();
    my $filename_c   = $filename . q{.c};
    my $filename_exe = $filename . $Config{_exe};
    $expected =~ s|FILE|$filename_c|gm;
    open my $fh, q{>}, $filename_c;
    print {$fh} $code;
    close $fh;
    system( $Config{cc}, q{-I.}, $filename_c, q{-o}, $filename_exe ) == 0
      or die "cannot compile C\n";

  EXERCISE:
    my $output = qx{$filename_exe};

  VERIFY:
    is( $output, $expected, $name );
    is( $? == 0, !!$success );

  TEARDOWN:
    unlink $filename_c;
    unlink $filename_exe;

    return;
}

tap_output_is( <<'CODE', <<'TAP', 0, 'bail_out' );
#include "tap.h"

int main() {
    bail_out("GOOD REASON");
}
CODE
Bail out!  GOOD REASON
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'bail_out without reason' );
#include "tap.h"

int main() {
    bail_out(NULL);
}
CODE
Bail out!
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'single line diag' );
#include "tap.h"

int main() {
    diag("foo, bar");
    return 0;
}
CODE
# foo, bar
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'multiple lines diag' );
#include "tap.h"

int main() {
    diag("foo\nbar\nbaz");
    return 0;
}
CODE
# foo
# bar
# baz
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'need plan' );
#include "tap.h"

int main() {
    PASS("foo");
    diag("We should never get here");
    return exit_status();
}
CODE
You tried to run a test without a plan
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'no_plan' );
#include "tap.h"

int main() {
    no_plan();
    PASS("foo");
    done_testing(1);
    return exit_status();
}
CODE
ok 1 - foo
1..1
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'bad plan' );
#include "tap.h"

int main() {
    plan(5);
    PASS("foo");
    PASS("bar");
    return exit_status();
}
CODE
1..5
ok 1 - foo
ok 2 - bar
# Looks like you planned 5 tests but ran 2.
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'duplicated plan' );
#include "tap.h"

int main() {
    plan(42);
    plan(42);
    diag("We should never get here");
    return exit_status();
}
CODE
1..42
You tried to plan twice
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'OK' );
#include "tap.h"

int main() {
    plan(2);
    OK(1, "foo");
    OK(1 == 1, "bar");
    return exit_status();
}
CODE
1..2
ok 1 - foo
ok 2 - bar
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'NOK' );
#include "tap.h"

int main() {
    plan(2);
    NOK(0, "foo");
    NOK(0 != 0, "bar");
    return exit_status();
}
CODE
1..2
ok 1 - foo
ok 2 - bar
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'PASS' );
#include "tap.h"

int main() {
    plan(2);
    PASS("foo");
    PASS("bar");
    return exit_status();
}
CODE
1..2
ok 1 - foo
ok 2 - bar
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'FAIL' );
#include "tap.h"

int main() {
    plan(2);
    FAIL("foo");
    FAIL("bar");
    return exit_status();
}
CODE
1..2
not ok 1 - foo
#    Failed test (FILE at line 5)
not ok 2 - bar
#    Failed test (FILE at line 6)
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'skip_all' );
#include "tap.h"

int main() {
    skip_all("GOOD REASON");
    return exit_status();
}
CODE
1..0 # SKIP GOOD REASON
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'skip' );
#include "tap.h"

int main() {
    plan(4);

    if (0) {
        skip("We're not skipping", 2);
    }
    else {
        PASS("not skipped in this branch");
        PASS("not skipped again");
    }

    if (1) {
        skip("Just testing the skip interface.", 2);
    }
    else {
        FAIL("Deliberate failure");
        FAIL("And again");
    }
    return exit_status();
}
CODE
1..4
ok 1 - not skipped in this branch
ok 2 - not skipped again
ok 3 - # skip Just testing the skip interface.
ok 4 - # skip Just testing the skip interface.
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'skip_rest' );
#include "tap.h"

int main() {
    plan(4);

    PASS("foo");
    PASS("bar");

    if (1) {
        skip_rest("Just testing the skip_rest interface.");
    }
    else {
        FAIL("Deliberate failure");
        FAIL("And again");
    }
    return exit_status();
}
CODE
1..4
ok 1 - foo
ok 2 - bar
ok 3 - # skip Just testing the skip_rest interface.
ok 4 - # skip Just testing the skip_rest interface.
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'todo' );
#include "tap.h"

int main() {
    plan(7);

    todo("Just testing the todo interface", 2);
    FAIL("Expected failure");
    FAIL("Another expected failure");

    PASS("This is not todo");

    todo("Just testing the todo interface", 1);
    FAIL("Yet another failure");

    PASS("This is not todo");

    if (1) {
        todo_skip("Just testing todo_skip");
    }
    else {
        FAIL("Just not reachable branch");
    }

    PASS("Again ok");
    return exit_status();
}
CODE
1..7
not ok 1 - Expected failure # TODO # Just testing the todo interface
#    Failed (TODO) test (FILE at line 7)
not ok 2 - Another expected failure # TODO # Just testing the todo interface
#    Failed (TODO) test (FILE at line 8)
ok 3 - This is not todo
not ok 4 - Yet another failure # TODO # Just testing the todo interface
#    Failed (TODO) test (FILE at line 13)
ok 5 - This is not todo
not ok 6 - # TODO & SKIP Just testing todo_skip
ok 7 - Again ok
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'INT_IS' );
#include "tap_helpers.h"

int main() {
    plan(2);
    INT_IS(42, 42, "eq");
    INT_IS(42, -43, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 6)
#         got: 42
#    expected: -43
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'LONG_IS' );
#include "tap_helpers.h"

int main() {
    plan(2);
    LONG_IS(42L, 42L, "eq");
    LONG_IS(42L, -43L, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 6)
#         got: 42
#    expected: -43
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 1, 'PTR_IS' );
#include "tap_helpers.h"

int main() {
    plan(1);
    PTR_IS(main, main, "eq");
    return exit_status();
}
CODE
1..1
ok 1 - eq
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'SIZE_IS' );
#include "tap_helpers.h"

int main() {
    plan(2);
    SIZE_IS(42u, 42u, "eq");
    SIZE_IS(42u, 40u, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 6)
#         got: 42
#    expected: 40
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'STRING_IS' );
#include "tap_helpers.h"

int main() {
    plan(3);
    STRING_IS("foo", "foo", "eq");
    STRING_IS("bar", "baz", "ne");
    STRING_IS("foo\nbar", "foo\nbaz", "ne with newline");
    return exit_status();
}
CODE
1..3
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 6)
#         got: bar
#    expected: baz
not ok 3 - ne with newline
#    Failed test (FILE at line 7)
#         got: foo
# bar
#    expected: foo
# baz
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'UINT_IS' );
#include "tap_helpers.h"

int main() {
    plan(2);
    UINT_IS(42, 42, "eq");
    UINT_IS(42, 40, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 6)
#         got: 0x2A (42)
#    expected: 0x28 (40)
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'ULONG_IS' );
#include "tap_helpers.h"

int main() {
    plan(2);
    ULONG_IS(42L, 42L, "eq");
    ULONG_IS(42L, 40L, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 6)
#         got: 0x2A (42)
#    expected: 0x28 (40)
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'PTR_NOT_NULL' );
#include "tap_helpers.h"

int main() {
    plan(2);
    PTR_NOT_NULL((void *)42u, "not null");
    PTR_NOT_NULL((void *)0u, "null");
    return exit_status();
}
CODE
1..2
ok 1 - not null
not ok 2 - null
#    Failed test (FILE at line 6)
#         got: NULL
#    expected: anything else
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'BOOL_IS' );
#include <stdbool.h>
#include "tap_helpers.h"

int main() {
    plan(4);
    INT_IS(true, true, "eq");
    INT_IS(false, false, "eq");
    INT_IS(true, false, "ne");
    INT_IS(false, true, "ne");
    return exit_status();
}
CODE
1..4
ok 1 - eq
ok 2 - eq
not ok 3 - ne
#    Failed test (FILE at line 8)
#         got: 1
#    expected: 0
not ok 4 - ne
#    Failed test (FILE at line 9)
#         got: 0
#    expected: 1
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'INT8_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    INT8_IS(42, 42, "eq");
    INT8_IS(42, -43, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 42
#    expected: -43
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'INT16_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    INT16_IS(42, 42, "eq");
    INT16_IS(42, -43, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 42
#    expected: -43
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'INT32_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    INT32_IS(42L, 42L, "eq");
    INT32_IS(42L, -43L, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 42
#    expected: -43
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'INT64_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    INT64_IS(42L, 42L, "eq");
    INT64_IS(42L, -43L, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 42
#    expected: -43
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'UINT8_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    UINT8_IS(42u, 42u, "eq");
    UINT8_IS(42u, 40u, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 0x2a (42)
#    expected: 0x28 (40)
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'UINT16_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    UINT16_IS(42u, 42u, "eq");
    UINT16_IS(42u, 40u, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 0x002a (42)
#    expected: 0x0028 (40)
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'UINT32_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    UINT32_IS(42uL, 42uL, "eq");
    UINT32_IS(42uL, 40uL, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 0x0000002a (42)
#    expected: 0x00000028 (40)
# Done with tap4embedded.
TAP

tap_output_is( <<'CODE', <<'TAP', 0, 'UINT64_IS' );
#define TAP_C99
#include "tap_helpers.h"

int main() {
    plan(2);
    UINT64_IS(42uL, 42uL, "eq");
    UINT64_IS(42uL, 40uL, "ne");
    return exit_status();
}
CODE
1..2
ok 1 - eq
not ok 2 - ne
#    Failed test (FILE at line 7)
#         got: 0x000000000000002a (42)
#    expected: 0x0000000000000028 (40)
# Done with tap4embedded.
TAP

=pod

=head1 NAME

test.pl

=head1 SYNOPSIS

prove test.pl

=head1 DESCRIPTION

Test suite for L<tap4embedded|http://github.com/fperrad/tap4embedded>.

=head1 LICENSE

Copyright (C) 2016-2017 Francois Perrad.

tap4embedded is free software; you can redistribute it and/or modify it
under the terms of the Artistic License 2.0

=cut
