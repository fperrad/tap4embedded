
# Host Linux

Verify the availability of the following *dev* tools:

    $ which make
    $ which gcc
    $ which perl
    $ which prove

In the directory `example/t.linux`, you should obtain:

    $ make build
    CCLD    00-happy.out
    CCLD    11-strncpy.out
    CCLD    12-strlcpy.out

    $ make test
    prove   --exec '' ./*.out
    ./00-happy.out .... ok
    ./11-strncpy.out .. ok
    ./12-strlcpy.out .. ok
    All tests successful.
    Files=3, Tests=16,  0 wallclock secs ( 0.01 usr +  0.01 sys =  0.02 CPU)
    Result: PASS

The option `--archive` of `prove` requires an additional Perl module `TAP::Harness::Archive`.

### with TinyCC

Verify the availability of [TinyCC](http://tinycc.org/):

    $ which tcc

In the directory `example/t.tcc`, you should obtain:

    $ tcc -run -I ../.. 00-happy.c
    1..3
    # Happy testing.
    ok 1 - Happy !
    ok 2 - Happy !!
    ok 3 - Happy !!!
    # Done with tap4embedded.

    $ make test
    prove   --exec 'tcc -run -g  -I . -I ../src -I ../..' ./*.c
    ./00-happy.c .... ok
    ./11-strncpy.c .. ok
    ./12-strlcpy.c .. ok
    All tests successful.
    Files=3, Tests=16,  0 wallclock secs ( 0.02 usr +  0.00 sys =  0.02 CPU)
    Result: PASS

# Host Windows

First, install a [StrawberryPerl](http://strawberryperl.com/) distribution which gives you :

- `C:\Strawberry\perl\bin\perl.exe`
- `C:\Strawberry\perl\bin\cpan.bat`
- `C:\Strawberry\perl\bin\prove.bat`
- `C:\Strawberry\c\bin\gmake.exe`
- `C:\Strawberry\c\bin\gcc.exe`

In the directory `example/t.windows`, you should obtain:

    > gmake build
    CCLD    00-happy.exe
    CCLD    11-strncpy.exe
    CCLD    12-strlcpy.exe

    > gmake test
    prove   --exec=call *.exe
    00-happy.exe .... ok
    11-strncpy.exe .. ok
    12-strlcpy.exe .. ok
    All tests successful.
    Files=3, Tests=16,  1 wallclock secs ( 0.05 usr +  0.02 sys =  0.06 CPU)
    Result: PASS

The option `--archive` of `prove` requires an additional Perl module `TAP::Harness::Archive`.
The following command install it:

    > cpan TAP::Harness::Archive

# Target Arduino UNO

The example is provided for the ubiquitous board [Arduino UNO](https://www.arduino.cc/en/Main/ArduinoBoardUno).

Install the [Arduino IDE](https://www.arduino.cc/en/Main/Software) which comes with :

- `avrdude` - utility to upload the ROM of AVR microcontrollers
- `avr-libc` - standard library for the AVR microcontrollers
- `gcc-avr` - compiler for the AVR microcontrollers

In order to verify your setup, from the Arduino IDE, try to run the example `04-Communication/ASCIITable`.

The Python (2.7 or 3.x) script which captures the output stream of the target, requires the additonnal module `pyserial`.

### from Linux

In the directory `example/t.linux`, you should obtain:

    $ make xbuild
    CCLD    00-happy.elf
    OBJCOPY 00-happy.hex
    CCLD    11-strncpy.elf
    OBJCOPY 11-strncpy.hex
    CCLD    12-strlcpy.elf
    OBJCOPY 12-strlcpy.hex

Usually, the port of the USB adapter is `/dev/ttyACM0`.

    $ ./ontarget.py 00-happy.hex
    # Uploading to target ...
    # /usr/share/arduino/hardware/tools/avrdude -C/usr/share/arduino/hardware/tools/avrdude.conf -q -q -patmega328p -carduino -P/dev/ttyACM0 -b115200 -D -Uflash:w:00-happy.hex:i
    # Running on target ...
    1..3
    # Happy testing.
    ok 1 - Happy !
    ok 2 - Happy !!
    ok 3 - Happy !!!
    # Done with tap4embedded.

    $ make xtest
    prove   --exec=./ontarget.py *.hex
    00-happy.hex .... ok
    11-strncpy.hex .. ok
    12-strlcpy.hex .. ok
    All tests successful.
    Files=3, Tests=16,  9 wallclock secs ( 0.02 usr  0.00 sys +  0.13 cusr  0.06 csys =  0.21 CPU)
    Result: PASS

### from Windows

In the directory `example/t.windows`, you should obtain:

    > gmake xbuild
    CCLD    00-happy.elf
    OBJCOPY 00-happy.hex
    CCLD    11-strncpy.elf
    OBJCOPY 11-strncpy.hex
    CCLD    12-strlcpy.elf
    OBJCOPY 12-strlcpy.hex

You should find the port COM used by the USB adapter, and set the variable `PORT_COM`.

    > set PORT_COM=COM7
    > ontarget.bat 00-happy.hex
    # Uploading to target ...
    # C:/Program Files (x86)/Arduino/hardware/tools/avr/bin/avrdude.exe -CC:/Program Files (x86)/Arduino/hardware/tools/avr/etc/avrdude.conf -q -q -patmega328p -carduino -PCOM7 -b115200 -D -Uflash:w:00-happy.hex:i
    # Running on target ...
    1..3
    # Happy testing.
    ok 1 - Happy !
    ok 2 - Happy !!
    ok 3 - Happy !!!
    # Done with tap4embedded.

    > gmake xtest
    prove   --exec=ontarget.bat *.hex
    00-happy.hex .... ok
    11-strncpy.hex .. ok
    12-strlcpy.hex .. ok
    All tests successful.
    Files=3, Tests=16,  9 wallclock secs ( 0.02 usr  0.00 sys +  0.13 cusr  0.06 csys =  0.21 CPU)
    Result: PASS
